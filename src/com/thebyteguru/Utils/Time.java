package com.thebyteguru.Utils;

/**
 * Created by omfg on 24.11.16.
 */
public class Time {
    public static final long SECOND = 1000000000L;

    public static long get(){
        return System.nanoTime();
    }
}
