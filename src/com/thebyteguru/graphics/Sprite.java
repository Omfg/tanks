package com.thebyteguru.graphics;

import com.thebyteguru.Utils.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by omfg on 28.11.2016.
 */
public class Sprite {
private float scale;
    private SpriteSheet sheet;
    BufferedImage image;

    public Sprite(SpriteSheet sheet,int scale){
        this.sheet=sheet;
        this.scale=scale;
         image=sheet.getSprite(0);
         image= Utils.resize(image, (int)(image.getWidth()*scale),
                 (int)(image.getHeight()*scale));
    }
    public void render(Graphics2D g, float x,float y){

        g.drawImage(image,(int) x,(int) y,null );
    }

}
