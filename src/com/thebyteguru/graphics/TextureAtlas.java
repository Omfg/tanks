package com.thebyteguru.graphics;

import com.thebyteguru.Utils.ResourceLoader;


import java.awt.image.BufferedImage;

/**
 * Created by omfg on 27.11.16.
 */
public class TextureAtlas {
    BufferedImage image;

    public TextureAtlas(String imageName){
        image = ResourceLoader.loadImage(imageName);
    }
public BufferedImage cut(int x, int y, int w, int h){
       return image.getSubimage(x,y,w,h);

}


}
